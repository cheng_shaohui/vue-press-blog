module.exports = {
  dest: 'public',
  base: '/vue-press-blog/',
  title: '程少辉的博客',
  description: '程少辉的个人博客',
  themeConfig: {
    logo: 'logo.jpg',
    nav: [
      {text: '主页', link: '/'},
      {text: '技术', link: '/technology/'},
      {text: '随笔', link: '/essay/'},
      {text: 'GitHub', link: '/https://github.com/chengshaohui/'}
    ]
  }
}
